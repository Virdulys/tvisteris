package lt.inntect;


import lt.inntect.category.IntegrationTests;
import org.junit.experimental.categories.Category;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.function.Supplier;

@Category(IntegrationTests.class)
public abstract class IntegrationTestBase extends TvisterisContextTests {

    @PersistenceContext
    protected EntityManager em;

    protected void flush(Runnable runnable) {
        runnable.run();
        flush();
    }

    protected void flush() {
        em.flush();
        em.clear();
    }

    protected <T> T flushAndGet(Supplier<T> supplier) {
        T result = supplier.get();
        flush();
        return result;
    }
}


