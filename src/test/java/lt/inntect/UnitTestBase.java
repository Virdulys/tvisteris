package lt.inntect;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lt.inntect.category.UnitTests;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.argThat;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTests.class)
public abstract class UnitTestBase {

    private Validator jsrValidator;
    protected ObjectMapper objectMapper;

    @Before
    public final void setUpBase() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        jsrValidator = factory.getValidator();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @SuppressWarnings("unchecked")
    public static class PredicateMatcher<T> extends ArgumentMatcher<T> {

        private final ArgumentMatcher<T> matcher;


        private PredicateMatcher(final Predicate<T> predicate) {
            this.matcher = new ArgumentMatcher<T>() {
                public boolean matches(Object argument) {
                    return predicate.test((T) argument);
                }
            };
        }

        @Override
        public boolean matches(Object argument) {
            return matcher.matches(argument);
        }

        public static <T> T anyMatching(Class<T> clazz, Predicate<T> predicate) {
            assertNotNull(clazz);
            assertNotNull(predicate);
            return argThat(new PredicateMatcher<>(predicate));
        }

        public static <E> List<E> anyListMatching(Class<E> clazz, Predicate<List<E>> predicate) {
            assertNotNull(clazz);
            assertNotNull(predicate);
            return argThat(new PredicateMatcher<>(predicate));
        }
    }

    public static <T> Answer<T> returnsFirstArgumentAs(Class<T> clazz) {
        return invocation -> invocation.getArgumentAt(0, clazz);
    }

    public static <T, R> Answer<R> returnsFirstArgumentAs(Class<T> clazz, Function<T, R> transformer) {
        return invocation -> transformer.apply(invocation.getArgumentAt(0, clazz));
    }

    public static <T> Answer<T> toAnswer(Supplier<T> supplier) {
        return invocation -> supplier.get();
    }
}
