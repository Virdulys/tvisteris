package lt.inntect;

import lt.inntect.category.IntegrationTests;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TvisterisApplication.class)
@Category(IntegrationTests.class)
@ActiveProfiles("test")
@WebAppConfiguration
public abstract class TvisterisContextTests extends AbstractTransactionalJUnit4SpringContextTests {
}
