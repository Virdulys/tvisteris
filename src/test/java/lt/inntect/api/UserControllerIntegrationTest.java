package lt.inntect.api;

import lt.inntect.api.data.UserDTO;
import lt.inntect.component.user.UserComponent;
import lt.inntect.component.user.data.User;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mapping.model.IllegalMappingException;
import org.springframework.http.MediaType;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerIntegrationTest extends WebIntegrationTestBase {

    @Inject
    private UserComponent userComponent;

    @Test
    public void givenFilledUserData_whenPosting_thenReturnNewUserId() throws Exception {
        UserDTO userDTO = new UserDTO("Monika", "Šaltiena");

        String response = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDTO)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat(response).isNotEmpty();
    }

    @Test
    public void givenUserRelatedExceptionOccurs_whenPosting_thenReturnExceptionMessage() throws Exception {
        String message = "blahblah";
        MockitoAnnotations.initMocks(this);
        Mockito.doThrow(new IllegalArgumentException(message)).when(userComponent).create(any(User.class));
        UserDTO userDTO = new UserDTO("Monika", "Šaltiena");

        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(message));
    }
}
