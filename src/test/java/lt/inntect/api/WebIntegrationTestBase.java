package lt.inntect.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.inntect.IntegrationTestBase;
import org.junit.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import javax.inject.Inject;

public abstract class WebIntegrationTestBase extends IntegrationTestBase {

    @Resource
    private WebApplicationContext context;

    @Inject
    protected ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @Before
    public final void setUpWebIntegrationTestBase() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
}