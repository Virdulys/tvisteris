package lt.inntect.component.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.inject.Inject;

import static org.mockito.Mockito.spy;

@Configuration
public class UserComponentSpyConfiguration {

    @Primary
    @Bean
    @Inject
    public UserComponent userComponent(UserRepository userRepository) {
        return spy(new UserComponentImpl(userRepository));
    }
}