package lt.inntect.component.user;

import lt.inntect.UnitTestBase;
import lt.inntect.component.user.data.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class UserComponentTest extends UnitTestBase {

    private UserComponent userComponent;
    @Mock
    private UserRepository userRepository;

    @Before
    public void setup() {
        userComponent = new UserComponentImpl(userRepository);
        LocalStubFactory.stubUserRepository(userRepository);
    }

    @Test
    public void givenFilledUserWithNullId_whenCreating_thenReturnUserWithId() {
        User user = new User(null, "Monika", "Šaltiena");
        assertThat(userComponent.create(user).getId()).isNotNull();
    }

    @Test
    public void givenFilledUserWithNullId_whenCreating_thenReturnUserWithSameData() {
        User user = new User(null, "Monika", "Šaltiena");
        assertThat(userComponent.create(user)).isNotNull()
                .isEqualToIgnoringGivenFields(user, "id");
    }

    @Test
    public void givenFilledUserWithNotNullId_whenCreating_thenThrowException() {
        User user = new User(5L, "Monika", "Šaltiena");
        Throwable e = catchThrowable(() -> userComponent.create(user));
        assertThat(e).isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage(UserError.ID_MUST_BE_NULL);
    }

    @Test
    public void givenUserLimitIsReached_whenCreating_thenThrowException() {
        when(userRepository.count()).thenReturn(UserComponent.MAX_USER_COUNT);

        User user = new User(null, "Monika", "Šaltiena");
        Throwable e = catchThrowable(() -> userComponent.create(user));
        assertThat(e).isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage(UserError.USER_LIMIT_REACHED);
    }

    private static class LocalStubFactory {
        static void stubUserRepository(UserRepository userRepository) {
            when(userRepository.save(any(User.class)))
                    .thenAnswer(returnsFirstArgumentAs(User.class, u -> {
                        if (u.getId() == null) u.setId(1L);
                        return u;
                    }));
        }
    }
}
