package lt.inntect.util;
 
import org.modelmapper.internal.util.Assert;
 
import java.util.Collection;
import java.util.function.Supplier;
 
abstract class CollectionMapper {
 
    static <S, SC extends Collection<S>, D, DC extends Collection<D>> DC map(SC source, Supplier<DC> destination, Class<D> destinationType) {
        DC dest = Assert.notNull(destination.get());
        if (source != null && source.size() > 0) {
            for (S obj : source) {
                dest.add(Mapper.map(obj, destinationType));
            }
        }
        return dest;
    }
 
    static <S, SC extends Collection<S>, D, DC extends Collection<D>> DC mapOrDefault(SC source, Supplier<DC> destination, Class<D> destinationType, DC defaultValue) {
        return isNotEmpty(source) ? map(source, destination, destinationType) : defaultValue;
    }
 
    private static boolean isNotEmpty(Collection<?> col) {
        return col != null && col.size() > 0;
    }
}