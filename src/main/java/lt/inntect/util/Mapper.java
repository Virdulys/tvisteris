package lt.inntect.util;

import org.hibernate.bytecode.instrumentation.internal.FieldInterceptionHelper;
import org.hibernate.bytecode.instrumentation.spi.FieldInterceptor;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.proxy.HibernateProxy;
import org.modelmapper.AbstractCondition;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NamingConventions;
import org.modelmapper.spi.MappingContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Mapper {

    private static ModelMapper mapper;

    static {
        mapper = initDefaultMapper();
    }

    private static ModelMapper initDefaultMapper() {
        ModelMapper mapper = new ModelMapper();

        Configuration config = mapper.getConfiguration();
        config.setMatchingStrategy(MatchingStrategies.STRICT);
        config.setFieldMatchingEnabled(false);
        config.setSourceNamingConvention(NamingConventions.JAVABEANS_ACCESSOR);
        config.setDestinationNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
        config.setMethodAccessLevel(Configuration.AccessLevel.PACKAGE_PRIVATE);

        config.setPropertyCondition(new AbstractCondition<Object, Object>() {
            public boolean applies(MappingContext<Object, Object> context) {
                Object source = context.getSource();
                if (source instanceof HibernateProxy) {
                    return !((HibernateProxy) source).getHibernateLazyInitializer().isUninitialized();
                } else if (FieldInterceptionHelper.isInstrumented(source)) {
                    FieldInterceptor interceptor = FieldInterceptionHelper.extractFieldInterceptor(source);
                    return interceptor == null || interceptor.isInitialized();
                } else if (source instanceof PersistentCollection) {
                    return ((PersistentCollection) source).wasInitialized();
                } else {
                    return true;
                }
            }
        });
        return mapper;
    }

    public static <S, D> D map(S source, Class<D> destinationType) {
        return mapper.map(source, destinationType);
    }

    public static <S, D> D mapOrDefault(S source, Class<D> destinationType, D defaultValue) {
        return source != null ? map(source, destinationType) : defaultValue;
    }

    public static <S, D> List<D> mapList(List<S> source, Class<D> destinationType) {
        return CollectionMapper.map(source, ArrayList::new, destinationType);
    }

    public static <S, D> List<D> mapListOrDefault(List<S> source, Class<D> destinationType, List<D> defaultValue) {
        return CollectionMapper.mapOrDefault(source, ArrayList::new, destinationType, defaultValue);
    }

    public static <S, D> Set<D> mapSet(Set<S> source, Class<D> destinationType) {
        return CollectionMapper.map(source, HashSet::new, destinationType);
    }

    public static <S, D> Set<D> mapSetOrDefault(Set<S> source, Class<D> destinationType, Set<D> defaultValue) {
        return CollectionMapper.mapOrDefault(source, HashSet::new, destinationType, defaultValue);
    }
}