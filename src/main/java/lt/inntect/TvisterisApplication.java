package lt.inntect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvisterisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvisterisApplication.class, args);
	}
}
