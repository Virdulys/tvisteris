package lt.inntect.api;

import lt.inntect.api.data.UserDTO;
import lt.inntect.component.user.UserComponent;
import lt.inntect.component.user.data.User;
import lt.inntect.util.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserComponent userComponent;

    @Inject
    public UserController(UserComponent userComponent) {
        this.userComponent = userComponent;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public Long create(@RequestBody UserDTO userDTO) {
        return userComponent.create(Mapper.map(userDTO, User.class)).getId();
    }
}
