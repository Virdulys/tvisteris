package lt.inntect.component.user;

import lt.inntect.component.user.data.User;

public interface UserComponent {
    Long MAX_USER_COUNT = 100L;

    User create(User user);
}
