package lt.inntect.component.user;

import lt.inntect.component.user.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

interface UserRepository extends JpaRepository<User, String> {
}
