package lt.inntect.component.user;

public abstract class UserError {
    public static final String ID_MUST_BE_NULL = "ID_MUST_BE_NULL";
    public static final String USER_LIMIT_REACHED = "USER_LIMIT_REACHED";
}
