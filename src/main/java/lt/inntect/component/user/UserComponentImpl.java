package lt.inntect.component.user;

import lt.inntect.component.user.data.User;
import org.modelmapper.internal.util.Assert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Transactional(readOnly = true)
@Component
class UserComponentImpl implements UserComponent {

    private UserRepository userRepository;

    @Inject
    public UserComponentImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = false)
    public User create(User user) {
        Assert.isNull(user.getId(), UserError.ID_MUST_BE_NULL);
        Assert.isTrue(userRepository.count() < MAX_USER_COUNT, UserError.USER_LIMIT_REACHED);
        return userRepository.save(user);
    }
}
